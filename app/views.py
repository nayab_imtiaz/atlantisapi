from django.shortcuts import render
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from django.http import Http404
from django.contrib.auth.models import User
from rest_framework.views import APIView
from .models import *
from .serializers import *
# Create your views here.
#profesionales/
class ProfesionalesList(APIView):


    def get_object(self, pk):
        try:
            return User.objects.get(username=pk)
        except User.DoesNotExist:
            raise Http404

    def get(self, request, pk ):
        usuarios = Profesionales.objects.all()
        usuarioSerializer = ProfesionalesSerializer(usuarios, many=True)
        return Response(usuarioSerializer.data)

    def post(self, request, pk):
        profesionalSerializer = ProfesionalesSerializer(data=request.data)
        if profesionalSerializer.is_valid():
            print(profesionalSerializer)
            print('now all')
            profesionalSerializer.save()
            return Response(profesionalSerializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(profesionalSerializer.errors, status=status.HTTP_400_BAD_REQUEST)


    def put(self, request, pk, format=None):
        print('in api')
        print(pk)
        instance = Profesionales.objects.get(pk=pk)
        # instance = User.objects.get(pk=pk)
        # instance = self.get_object(pk)
        # print(request.data.get('user'))
        # id= request.data.get('user').get('id')
        # print(instance)
        # user= User.objects.get(pk=3)
        # print(user.id)
        # print(user.email)
        print('priniting instance')
        print(instance)
        print(instance.id)
        # data = JSONParser().parse(request.data)
        profesionalSerializer = ProfesionalesSerializer(instance, data=request.data, partial=True)
        # profesionalSerializer = ProfesionalesSerializer(instance, data=data)
        print(profesionalSerializer)
        if profesionalSerializer.is_valid():
             profesionalSerializer.save()
             return Response(profesionalSerializer.data, status=status.HTTP_201_CREATED)
        else:
            print(profesionalSerializer.errors)
            print('here')
            return Response(profesionalSerializer.errors, status=status.HTTP_400_BAD_REQUEST)