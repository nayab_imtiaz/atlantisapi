from django.shortcuts import render
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from django.http import Http404
from django.contrib.auth.models import User
from rest_framework.views import APIView
from rest_framework import  serializers

from .models import *
#
#
# class UsernameValidator(object):
#
#     def set_context(self, serializer_field):
#         """
#         This hook is called by the serializer instance,
#         prior to the validation call being made.
#         """
#         # Determine the existing instance, if this is an update operation.
#         self.instance = getattr(serializer_field.parent, 'instance', None)
#         if not self.instance:
#             # try to get user from profesionales:
#             root_instance = getattr(serializer_field.root, 'instance', None)
#             self.instance = getattr(root_instance, 'user', None)
#
#     def __call__(self, value):
#         if self.instance and User.objects.filter(username=value).exclude(id=self.instance.id).exists():
#             raise ValidationError('Username already exists.')
#
#         if not self.instance and User.objects.filter(username=value).exists():
#             raise ValidationError('Username already exists.')


class UserSerializer(serializers.ModelSerializer):
    print('in serializer user')
    # username = serializers.CharField(max_length=128, validators=[UsernameValidator()])

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'id')
        extra_kwargs = {
            'username': {'validators': []},
        }

        # i have towrite custom create and update method to check validation of username for create but remove validation for update

class ProfesionalesSerializer(serializers.ModelSerializer):
    print('in ser professional')
    user = UserSerializer()
    numColegiado = serializers.CharField(allow_blank=False)

    class Meta:
        model = Profesionales
        fields = ('user', 'numColegiado')

    def create(self, validated_data):
        print('in serializer create')
        user_data = validated_data.pop('user')
        user = User.objects.create(**user_data)
        profesional = Profesionales.objects.create(user=user, **validated_data)
        return profesional

    def update(self, instance, validated_data):
        print('in ser update')
        user_data = validated_data.pop('user')
        user = instance.user
        # userSerializer = UserSerializer(user, data=user_data)
        userSerializer = UserSerializer(user, data=user_data)
        if userSerializer.is_valid(raise_exception=True):
            userSerializer.save()

        num_colegiado = validated_data.get('numColegiado')
        if num_colegiado:
            instance.numColegiado = num_colegiado
            instance.save()
        return instance