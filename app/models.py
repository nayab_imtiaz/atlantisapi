from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Profesionales(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    dni = models.CharField(max_length=9, blank=True, default='')
    numColegiado = models.CharField(max_length=8, blank=True, default='')
    valoracionMedia = models.FloatField(blank=True, default=0)
    numVotos = models.IntegerField(blank=True, default=0)

    def __str__(self):
        return self.numColegiado